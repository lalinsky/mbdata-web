# Copyright (C) 2013 Lukas Lalinsky
# Distributed under the MIT license, see the LICENSE file for details.

import os
from flask import Flask, render_template

app = Flask('mbdata_web')
app.config['MBDATA_API_URL'] = os.environ.get('MBDATA_API_URL', 'http://127.0.0.1:5000')


@app.route('/')
@app.route('/<path:path>')
def main(path=None):
    if path and path.startswith('/static/'):
        return app.send_static_file(path[8:])
    return render_template('index.html', api_url=app.config['MBDATA_API_URL'])


if __name__ == '__main__':
    app.run(port=5001, debug=True)

