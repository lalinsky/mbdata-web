MusicBrainz Database Tools (Demo Website)
=========================================

This package contains a simple JavaScript application written using AngularJS
to demo the `mbdata` HTTP API.

Installation:

    virtualenv --system-site-packages e
    . e/bin/activate
    pip install -r requirements.txt

Start the development server:

    #!sh
    MBDATA_API_URL='http://localhost:5000' python -m mbdata_web.py

